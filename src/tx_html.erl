% @doc
% HTML record type
-module(tx_html).

-export([
    doc/1,
    env/3,
    sa/2,
    pf_document/1,
    pf_htmls/1
]).

% Example of standalone
% <link rel="stylesheet" href="/foo.css">
-record(sa,
        {name  :: string(),
         attrs :: [{key(), value()}]}).

-type key() :: atom() | string().
-type value() :: string().

% Example of environment
% <div id="container">
%   ...
% </div>
-record(env,
        {name  :: string(),
         attrs :: [{string(), string()}],
         subs  :: htmls()}).

-type environment() :: #env{}.
-type standalone()  :: #sa{}.
-type text()        :: {text, string()}.
-type html()        :: environment() | standalone() | text().
-type htmls()       :: [html()].
-type document()    :: {document, htmls()}.

% @doc wrap everything in document tupel
doc(Htmls) ->
    {document, Htmls}.

% @doc construct environment-type tag
env(Name, Attrs, Subs) ->
    #env{name = Name, attrs = Attrs, subs = Subs}.

% @doc construct standalone tag
sa(Name, Attrs) ->
    #sa{name = Name, attrs = Attrs}.

pf_document({document, Htmls}) ->
    [pf_doctype5(), pf_htmls(Htmls)].

pf_doctype5() ->
    "<!DOCTYPE html>\n".

pf_htmls(Htmls) ->
    pf_htmls(Htmls, []).

pf_htmls([], FinalAcc) ->
    FinalAcc;
pf_htmls([Html | Rest], Acc) ->
    ThisStr = pf_html(Html),
    NewAcc = [Acc, ThisStr],
    pf_htmls(Rest, NewAcc).

pf_html(#sa{name = Name, attrs = Attrs}) ->
    ["<", Name, pf_attrs(Attrs), ">"];
pf_html(#env{name = Name, attrs = Attrs, subs = Subs}) ->
    OpenTag = ["<", Name, pf_attrs(Attrs), ">"],
    SubsStr = pf_htmls(Subs),
    CloseTag = ["</", Name, ">"],
    [OpenTag, SubsStr, CloseTag];
pf_html({text, Str}) ->
    Str.

pf_attrs(Attrs) ->
    pf_attrs(Attrs, []).

pf_attrs([], FinalAcc) ->
    FinalAcc;
pf_attrs([KV | Rest], Acc) ->
    KVStr = pf_attr(KV),
    NewAcc = [Acc, KVStr],
    pf_attrs(Rest, NewAcc).


pf_attr({Key, Value}) when is_atom(Key) ->
    KeyStr = erlang:atom_to_list(Key),
    pf_attr({KeyStr, Value});
pf_attr({Key, Value}) when is_list(Key) ->
    % this is just to emphasize that the space is intentional
    % the space is there to separate distinct key value pairs
    Space = " ",
    Result = [Space, Key, "=\"", Value, $"],
    Result.
