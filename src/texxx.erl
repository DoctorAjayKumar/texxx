%%% @doc
%%% TeXXX: texxx
%%%
%%% This module is currently named `texxx', but you may want to change that.
%%% Remember that changing the name in `-module()' below requires renaming
%%% this file, and it is recommended to run `zx update .app` in the main
%%% project directory to make sure the ebin/texxx.app file stays in
%%% sync with the project whenever you add, remove or rename a module.
%%% @end

-module(texxx).
-vsn("0.1.0").
-author("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-copyright("Dr. Ajay Kumar PHD <DoctorAjayKumar@protonmail.com>").
-license("MIT").

-export([hello/0]).


-spec hello() -> ok.

hello() ->
    io:format("~p (~p) says \"Hello!\"~n", [self(), ?MODULE]).
