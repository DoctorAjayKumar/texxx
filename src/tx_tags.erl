% @doc html record types for common tags
-module(tx_tags).

-compile(export_all).

example() ->
    doc([html([{"lang", "en"}],
              [head([title("Hello World")]),
               body([h1("Hello World")])])]).

bstemp() ->
    doc(bshtmls()).

bshtmls() ->
    [html([{"lang", "en"}],
          [head(bs_head_htmls()),
           body(bs_body_htmls())])].

bs_head_htmls() ->
    [meta_utf8_html(),
     meta_viewport_html(),
     stylesheet_html(),
     title("Hello World!")].

meta_utf8_html() ->
    meta([{charset, "utf-8"}]).

meta_viewport_html() ->
    meta([{name, "viewport"},
          {content, "width=device-width, initial-scale=1, shrink-to-fit=no"}]).

stylesheet_html() ->
    link_([{rel, "stylesheet"},
           {href, "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"},
           {integrity, "sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"},
           {crossorigin, "anonymous"}]).

bs_body_htmls() ->
    [h1("Hello World"),
     script([{"src", "https://code.jquery.com/jquery-3.5.1.slim.min.js"},
             {"integrity", "sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"},
             {"crossorigin", "anonymous"}]),
     script([{"src", "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"},
             {"integrity", "sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"},
             {"crossorigin", "anonymous"}])].


doc(Htmls) ->
    tx_html:doc(Htmls).

html(Tags, Subs) ->
    env("html", Tags, Subs).

head(Subs) ->
    env("head", [], Subs).

meta(Attrs) ->
    sa("meta", Attrs).

% link is a reserved term in faggot Erlang
link_(Attrs) ->
    sa("link", Attrs).

title(Text) ->
    env("title", [], [{text, Text}]).

body(Subs) ->
    env("body", [], Subs).

h1(Text) ->
    env("h1", [], [{text, Text}]).

script(Attrs) ->
    env("script", Attrs, []).

%%% internal

env(Name, Attrs, Subs) ->
    tx_html:env(Name, Attrs, Subs).

sa(Name, Attrs) ->
    tx_html:sa(Name, Attrs).

