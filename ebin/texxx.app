{application,texxx,
             [{description,"TeX-like language that compiles to HTML"},
              {registered,[]},
              {included_applications,[]},
              {applications,[stdlib,kernel]},
              {vsn,"0.1.0"},
              {modules,[texxx]}]}.
