Definitions.

ALPHANUM  = [A-Za-z]
COMMAND   = \\{ALPHANUM}+
ENV_OPEN  = \\begin\{{ALPHANUM}+\}

Rules.

{COMMAND}   :   {token, {bscmd, TokenLine, unbs(TokenChars)}}.
\\\{        :   {token, {char, TokenLine, chr("{")}}.
\\\}        :   {token, {char, TokenLine, chr("}")}}.
\{          :   {token, {open_brace, TokenLine}}.
\}          :   {token, {close_brace, TokenLine}}.
.           :   {token, {char, TokenLine, chr(TokenChars)}}.

Erlang code.

chr([X]) -> X.
unbs([$\\ | Rest]) -> Rest.
